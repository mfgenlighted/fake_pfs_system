﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fakepfs2
{
    public class PFSCalls
    {

        public int pfs_login (
	    string request,
	    ref string response,
	    int bufSize,
	    string address)
	    {
            var yn = MessageBox.Show(request, "login request", MessageBoxButtons.YesNo);
            if (yn == DialogResult.Yes)
            {
                response = "OK";
                return 0;
            }
            else
            {
                response =  "Unable to log in";
                return -1;
            }
	    }

        public int pfs_query (
	    string request,
	    ref string response,
	    int bufSize,
	    string address)
	    {
            var yn = MessageBox.Show(request, "query request", MessageBoxButtons.YesNo);
            if (yn == DialogResult.Yes)
            {
                response = "OK";
                return 0;
            }
            else
            {
                response = "Error";
                return -1;
            }

	    }

        public int pfs_send_results (
	    string request,
	    ref string response,
	    int bufSize,
	    string address)
	    {
            var yn = MessageBox.Show(request, "results request", MessageBoxButtons.YesNo);
            if (yn == DialogResult.Yes)
            {
                response = "OK";
                return 0;
            }
            else
            {
                response = "Error";
                return -1;
            }
        }


    }
}
